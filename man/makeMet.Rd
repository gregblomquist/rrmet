\name{makeMet}
\alias{makeMet}
\alias{met}
\alias{met.default}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Create a \code{met} object from data and information about groups.
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
Assemble a \code{met} object from a \code{data.frame} of measurements and \code{data.frame} of information (names, ids, census sizes) about groups the measurements were collected on. Group ids must match between the \code{data.frame}s.
}
\usage{
makeMet(dat, info, header = "Data for R-matrix analysis")
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{dat}{
%%     ~~Describe \code{dat} here~~
A \code{data.frame} of measurements on individuals. The first column should be "group" and be an integer code matching the codes in \code{info$id}.
}
  \item{info}{
%%     ~~Describe \code{info} here~~
A \code{data.frame} with 3 columns describing the groups measurements were collected on. The first column is a character label for the group. The second is a numeric id matching those used in \code{dat$group}. The last column is a census size for the group (\emph{not} sample size). 
}
  \item{header}{
%%     ~~Describe \code{header} here~~
A character description of the data set, defaulting to "Data for R-matrix analysis."
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
The function returns an object of class \code{met}. The function will fail if the numeric ids do not match between \code{info} and \code{dat}.
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
  \item{header}{The character description of the data set.}
  \item{g}{The number of groups.}
  \item{v}{The number of variables (i.e. measurements).}
  \item{info}{The input \code{info}.}
  \item{dat}{The input \code{dat}.}
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
\code{\link{readMet}}, \code{\link{writeMet}}, \code{\link{print.met}}, \code{\link{summary.met}}
}
\examples{
data(howells)

# make the group information table
# use the 6 continental region classifications as groups
# census sizes are unknown, make them all equal
group.names <- unique(howells$region)[order(unique(howells$region))]
group.ids   <- seq(1:length(group.names))
group.census <- rep(1,length(group.names))
group.info <- data.frame(group.names, group.ids, group.census, stringsAsFactors=FALSE)
str(group.info)

# now work on the variables
reg.data <- howells[,c(1, 3:dim(howells)[2])] # drop population
# make a vector of numeric ids matching codes in group.info
group <- match(reg.data$region, group.info$group.name) 
table(reg.data$region)
table(group)
# replace the group names with the ids now
group.data <- data.frame(group, reg.data[,2:dim(reg.data)[2]])
head(reg.data)
head(group.data)

# finally, makeMet() can build a met object
how6 <- makeMet(dat=group.data, info=group.info, header="Howells craniometrics, continental regions and variables of Relethford (1994)")
print(how6)
summary(how6)
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
