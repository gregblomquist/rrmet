\name{print.met}
\alias{print.met}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
%%  ~~function to do ... ~~
Describe the contents of a \code{met} object.
}
\description{
  %%  ~~ A concise (1-5 lines) description of what the function does. ~~
  Display some useful information about a \code{met} object including
  the header, information about groups and variables, and the first
  \code{n} lines of data.
}
\usage{
\method{print}{met}(x, n = 5, ...)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{x}{
    %%     ~~Describe \code{x} here~~
    An object of class \code{met} created by \code{makeMet()} or \code{readMet()}.
}
  \item{n}{
    %%     ~~Describe \code{n} here~~
    An integer controlling the number of initial lines of data to be
    printed. The default is 5.
}
  \item{\dots}{
%%     ~~Describe \code{\dots} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
  %% ~~objects to See Also as \code{\link{help}}, ~~~
  \code{\link{summary.met}}
}
\examples{
data(irish)
print(irish,n=3)
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
