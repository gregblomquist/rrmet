\name{writeMet}
\alias{writeMet}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
%%  ~~function to do ... ~~
Write a \code{met} object to disk for use with Windows RMET.
}
\description{
  %%  ~~ A concise (1-5 lines) description of what the function does. ~~
  Any \code{met} object in the workspace can be written to disk in a format
  compatible with Relethford's Windows RMET program. This facilitates
  cross-checking results between the programs, or simply using R to
  prepare data for use with RMET. This is the opposite of \code{readMet()}.
}
\usage{
writeMet(obj, filename)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{obj}{
    %%     ~~Describe \code{obj} here~~
    An object of class \code{met} created by \code{makeMet()} or
    \code{readMet()}. 
}
  \item{filename}{
    %%     ~~Describe \code{filename} here~~
    A quoted character path to the location where the file should be
    saved on disk. The file name extension ".met" should be included,
    but the function does not check for it. Errors will be caused if the
    user lacks write permissions to the location they are trying to save 
    the file. 
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
  %% ~~objects to See Also as \code{\link{help}}, ~~~
  \code{\link{makeMet}}, \code{\link{readMet}}
}
\examples{
\dontrun{data(irish); writeMet(irish,"mydata/irishondisk.met")}
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ file }

