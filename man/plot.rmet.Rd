\name{plot.rmet}
\alias{plot.rmet}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
%%  ~~function to do ... ~~
Plot principal coordinates of an \code{rmet} object's distance matrix.
}
\description{
  %% ~~ A concise (1-5 lines) description of what the function does. ~~
  The distance matrix derived from an R-matrix generated with
  \code{RRmet()} is visualized in two-dimensions by collapsing it into
  its first two principal coordinates.  The \code{rmet} object must have
  been generated with \code{pco=TRUE} for plotting to be available. }

\usage{ \method{plot}{rmet}(x, y = NULL, type = "p",
  xlim = NULL, ylim = NULL, log = "", main = NULL, sub = NULL, xlab =
  NULL, ylab = NULL, ann = par("ann"), axes = TRUE, frame.plot = axes,
  panel.first = NULL, panel.last = NULL, asp = 1, ..., what =
  "unscaled", pad = 0.1, ptlabel = TRUE) }
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{x}{
    %%     ~~Describe \code{x} here~~
    The \code{rmet} object to extract the distance matrix principal
    coordiantes and plot. 
}
  \item{y}{
    %%     ~~Describe \code{y} here~~
    See \code{help(plot.default)}.
}
  \item{type}{
    %%     ~~Describe \code{type} here~~
        See \code{help(plot.default)}.
}
  \item{xlim}{
    %%     ~~Describe \code{xlim} here~~
        See \code{help(plot.default)}.
}
  \item{ylim}{
    %%     ~~Describe \code{ylim} here~~
        See \code{help(plot.default)}.
}
  \item{log}{
    %%     ~~Describe \code{log} here~~
        See \code{help(plot.default)}.
}
  \item{main}{
    %%     ~~Describe \code{main} here~~
        See \code{help(plot.default)}.
}
  \item{sub}{
    %%     ~~Describe \code{sub} here~~
        See \code{help(plot.default)}.
}
  \item{xlab}{
    %%     ~~Describe \code{xlab} here~~
        See \code{help(plot.default)}.
}
  \item{ylab}{
    %%     ~~Describe \code{ylab} here~~
        See \code{help(plot.default)}.
}
  \item{ann}{
    %%     ~~Describe \code{ann} here~~
        See \code{help(plot.default)}.
}
  \item{axes}{
    %%     ~~Describe \code{axes} here~~
        See \code{help(plot.default)}.
}
  \item{frame.plot}{
    %%     ~~Describe \code{frame.plot} here~~
        See \code{help(plot.default)}.
}
  \item{panel.first}{
    %%     ~~Describe \code{panel.first} here~~
        See \code{help(plot.default)}.
}
  \item{panel.last}{
    %%     ~~Describe \code{panel.last} here~~
        See \code{help(plot.default)}.
}
  \item{asp}{
    %%     ~~Describe \code{asp} here~~
    The aspect ratio of the plot. Defaults to 1, which will give the
    proper visual representation of distances between points. 
}
  \item{\dots}{
    %%     ~~Describe \code{\dots} here~~
        See \code{help(plot.default)}.
}
  \item{what}{
    %%     ~~Describe \code{what} here~~
    Either \code{'scaled'} or \code{'unscaled'} identifying which
  distance matrix to use. Scaled uses weighting by census sizes
  described by Relethford (1996). 
}
  \item{pad}{
    %%     ~~Describe \code{pad} here~~
    A numeric value giving the amount of extra space to be added to the
    x and y-limits to prevent point labels (see \code{ptlabel}) from
    being cutoff by the plot boundaries. Default is 0.1.
}
  \item{ptlabel}{
    %%     ~~Describe \code{ptlabel} here~~
    A logical controlling whether text labels will identify points in
    the plot. The default is TRUE.
}
}
\details{
  %%  ~~ If necessary, more details than the description above ~~
  This function is fairly simple but provides some nice features like
  decoration of the axes with percents of variation explained and
  overlayed 0 axes to indicate the centroid. Users familiar with default
  \code{plot()} can manipulate the output extensively or tailor output
  more flexibly by plotting the \code{PCO.scaled} or \code{PCO.unscaled}
  \code{data.frame}s contained with an \code{rmet} object
  (e.g. \code{plot(rmet$PCO.unscaled)}).

  The principal coordinates analysis is performed by \code{RRmet()} using
  the \code{cmdscale()} function. The direction of the axes in these
  plots is arbitrary but the distances between points (and resulting
  inferences) should be consistent between R and Windows RMET. Often the
  x or y-axis will appear 'flipped' when comparing between the two programs.
}

\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
  %% ~put references to the literature/web site here ~
  Relethford, JH. 1996. Genetic drift can obscure population history: Problem and solution. \emph{Hum Biol} 68:29-44.
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
  %% ~~objects to See Also as \code{\link{help}}, ~~~
  \code{\link{RRmet}}
}
\examples{
data(irish)
plot(RRmet(irish,verbose=FALSE),what='scaled')
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
