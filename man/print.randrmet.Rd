\name{print.randrmet}
\alias{print.randrmet}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
%%  ~~function to do ... ~~
Describe the contents of a \code{randrmet} object.
}
\description{
  %%  ~~ A concise (1-5 lines) description of what the function does. ~~
  Displays the distances between groups and randomization p-values. More
  detailed information is given by \code{summary()}.
}
\usage{
\method{print}{randrmet}(x, ...)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{x}{
    %%     ~~Describe \code{x} here~~
    An object of class \code{randrmet} created by \code{randRRmet()}.
}
  \item{\dots}{
%%     ~~Describe \code{\dots} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
  %% ~~objects to See Also as \code{\link{help}}, ~~~
  \code{\link{summary.randrmet}}
}
\examples{
data(irish)
rmetobj <- RRmet(irish)
randrmetobj <- randRRmet(rmetobj,nrans=20) # low nrans to make this faster
print(randrmetobj)
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
