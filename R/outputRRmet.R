outputRRmet <-
function(RRmet.obj,printmat=TRUE){
# function for printing output from RRmet() in way that mimics Windows Rmet
# wrapped by summary.rmet() defined below
  
  if (class(RRmet.obj)!="rmet") stop('Input object is not of class "rmet". Run RRmet() first.')
  
  o <- RRmet.obj

  cat("\n")
  cat("Analysis of",dim(o$info)[1],"groups and",(dim(o$dat)[2]-1),"variables.","\n\n")
  eq <- "Unequal"
  if(min(o$w)==max(o$w)){eq <- "Equal"} 
  if ( sum( abs((o$info$census/sum(o$info$census)) - o$w)) > 0 ) {
    cat(eq,"weights specified by user with weights=c(x,y,z).","\n\n")
  }
  else {cat(eq,"group weights specified by info.","\n\n")}
  if (o$biascorrection) {cat("Bias correction for sample size differences among groups requested.","\n"," Any resulting r.ii<0 and d.ij<0 are set equal to 0.","\n\n")}
    else {cat("No bias correction for sample size differences among groups.","\n\n")}
  cat("Heritability =",o$h2,"\n\n")

  numtab <- data.frame(o$info,table(o$dat[,1]))
  names(numtab)[4:5] <- c("weight","sample")
  numtab$weight <- round(o$w,4)
  totline <- numtab[1,]
  totline[1,1] <- "Total"
  totline[1,3] <- sum(numtab$census)
  totline[1,5] <- sum(numtab$sample)
  totline$weight <- NA
  numtab <- rbind(numtab,totline)
  row.names(numtab) <- numtab[,1]
  print(as.matrix(numtab[,c(5,3,4)]),na.print=" ")


  
  cat("\n","Variables: ",names(o$dat)[2:length(names(o$dat))],"\n\n")

  cat("Analysis of R-matrix diagonals:","\n")
  r.diags <- round(data.frame(diag(as.matrix(o$R.biased)),diag(as.matrix(o$R.unbiased)),diag(as.matrix(o$se.r))),6)
  names(r.diags) <- c("r.ii.biased","r.ii.unbiased","se")
  print(r.diags)

  fst <- data.frame(o$Fst.biased,o$Fst.unbiased,o$se.Fst)
  names(fst) <- c("Fst.biased","Fst.unbiased","se")
  fst <- data.frame(t(fst))
  names(fst) <- " "
  print(fst)
  cat("\n\n\n")

  cat("Relethford-Blangero analysis:","\n")
  cat("Mean Within-group Phenotypic Variance =",o$vw[1,1],"\n")
  print(o$RelBlan)
  cat("\n\n\n")

  if ("PCO.unscaled" %in% names(o)){ # only print pco results if in RRmet() object
  cat("Principal Coordinates analysis (unscaled R-matrix):","\n")
  cat(length(o$lambda.G.unscaled),"non-zero eigenvalues:","\n")
  cat(round(o$lambda.G.unscaled,6),"\n")
  cat("1st eigenvalue accounts for",round(o$lambda.G.unscaled.2pct[1],3),"percent of variation","\n")
  cat("2nd eigenvalue accounts for",round(o$lambda.G.unscaled.2pct[2],3),"percent of variation","\n")
  cat("Together they account for  ",round(o$lambda.G.unscaled.tot,3),"percent of variation","\n\n")
  cat("First two eigenvectors scaled by the sqrt() of eigenvalue:.","\n")
  print(o$PCO.unscaled)
  cat("\n\n")

  # only print scaled values if groups of unequal weight
  if (min(o$w)!=max(o$w)){
  cat("Principal Coordinates analysis (scaled R-matrix):","\n")
  cat(length(o$lambda.G.scaled),"non-zero eigenvalues:","\n")
  cat(round(o$lambda.G.scaled,6),"\n")
  cat("1st eigenvalue accounts for",round(o$lambda.G.scaled.2pct[1],3),"percent of variation","\n")
  cat("2nd eigenvalue accounts for",round(o$lambda.G.scaled.2pct[2],3),"percent of variation","\n")
  cat("Together they account for  ",round(o$lambda.G.scaled.tot,3),"percent of variation","\n\n")
  cat("First two eigenvectors scaled by the sqrt() of eigenvalues:","\n")
  print(o$PCO.scaled)
  }

  cat("\n\n")
  cat(" Use plot() on the RRmet() output to visualize the principal coordinates.","\n")
#  cat(" or manually plot() the PCO.unscaled or PCO.scaled objects.","\n\n\n")

  } # end pco part

  
  # remaining item on Rmet output is lists of R-matrix and d-matrix
  # entries with SEs; I find matrices easier to interpret, but maybe
  # you should make this optional to have printed and optional how to
  # print it column v. matrix form; optional printing is implemented
  # with printmat but variable formatting is not

  if (printmat){

  # table with unbiased R-matrix on diagonal and in upper triangle
  # corresponding genetic distances in the lower triangle
  RDtab <- o$R.unbiased
  RDtab[lower.tri(RDtab,diag=FALSE)] <-  o$d[lower.tri(o$d,diag=FALSE)]
  # matching SEs for the matrix entries
  SEtab <- o$se.r
  SEtab[lower.tri(SEtab,diag=FALSE)] <-  o$se.d[lower.tri(o$se.d,diag=FALSE)]
  # interleaved for pretty printing
  RDSEtab <- data.frame(matrix(NA,nrow=dim(RDtab)[1],ncol=2*dim(RDtab)[2]))
  row.names(RDSEtab) <- row.names(RDtab)
  names(RDSEtab)[seq(1,dim(RDSEtab)[2],2)] <- names(RDtab)
  names(RDSEtab)[seq(2,dim(RDSEtab)[2],2)] <- rep(" ",length(seq(2,dim(RDSEtab)[2],2)))
  RDSEtab[,seq(1,dim(RDSEtab)[2],2)] <- RDtab
  RDSEtab[,seq(2,dim(RDSEtab)[2],2)] <- SEtab
  
  cat("Unscaled R-matrix (upper triangle and diagonal) and distance matrix (lower triangle) with standard errors:","\n")
  print(round(RDSEtab,6))
  cat("\n")

  if (min(o$w)!=max(o$w)){
      RDtab.s <- o$R.scaled
      RDtab.s[lower.tri(RDtab.s,diag=FALSE)] <-  o$d.scaled[lower.tri(o$d.scaled,diag=FALSE)]
      cat("Scaled R-matrix (upper triangle and diagonal) and distance matrix (lower triangle):","\n")
      print(round(RDtab.s,6))
    } 

  }
  
}

